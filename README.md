# Chat Test

- Creates random users when connecting to chat
- User is able to see other users connected and identify it self in the list of connected user through the ⭐️ emoji
- Bot messages when users connect or disconnect
- User can edit only their messages
  - To edit a message click on the message and you'll be able to edit the content. It will be saved when clicking again outside the message. A new icon appears next to the user message information indicating the message was edited to every other connected user.
- User can delete their messages.
  - To delete a message click on the message and on the right top corner of the message clik on the 🗑️ emoji.
  - Once a message is deleted it can be edited.
- Users are able to see other user is typing through a message on top of the input.

**Tested on**

```
Chrome Version 80.0.3987.163 (Mac macOS Catalina)
Firefox Developer Edition Version 72.0b9 (Mac macOS Catalina)
Chrome 80.0.3987.149 (Android 8.1.0;Moto G (5))
```

**Time tracking**

```
Mon April 27th.  - 3 hours
Tue April 28th.  - 3 hours
Wed April 29th.  - 2 hours
Thu April 30th.  - 4 hours
Sat May 2nd - 1 hour
```

# Running

Test requires [Node.js](https://nodejs.org/) to run.

Change the app and server config files in order to use your local IP.

For the WS server in `/server/server.config.js`, app server in `/app/src/js/app.config.js` and the config host in `/app/package.json`.

By default the App server runs in port _3000_ and WS server in _3001_.
You can also change the port values in the config files of the app and server.

First `npm install` to grab all the necessary dependencies.

**App dependencies**

```sh
$ cd app
$ npm install
```

**Server dependencies**

```sh
$ cd server
$ npm install
```

**Run Project**

```sh
$ cd app
$ npm start
```

**Run App Server**

```sh
$ cd app
$ npm run dev
```

**Run Node Server**

```sh
$ cd server
$ npm run server
```

**App Build**

```sh
$ cd app
$ npm run build
```

**App Tests**

```sh
$ cd app
$ npm run test
```

# Contact Email

andoni282@gmail.com

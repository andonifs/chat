module.exports = {
  transform: {
    '^.+\\.jsx?$': 'babel-jest',
  },
  projects: ['./src'],
  setupFiles: ['./src/js/__mocks__/sessionStorageMock'],
};

import {
  editMessage,
  enableActions,
  renderMessage,
  renderDeletedMessage,
  renderEditedMessage,
  renderUserIsTypingMessage,
} from '../components/Chat/actions';

import { connection, setListenersToChatInput } from '../components/Chat/index';

test('Renders a deleted message', () => {
  document.body.innerHTML = `
  <ul id="chatView">
    <li class="actions edited" id="000">
      <div class="chat__message-body" contenteditable="true">Hello</div>
    </li>
  </ul>`;

  const expected = `
    <li class="" id="000">
      <div class="chat__message-body bot-message" contenteditable="false">delete</div>
    </li>
  `;

  const chatView = document.getElementById('chatView');
  renderDeletedMessage({ messageId: '000', message: 'delete' });
  expect(chatView.innerHTML).toEqual(expected);
});

test('Renders an edited message', () => {
  document.body.innerHTML = `
  <ul id="chatView">
    <li class="chat__message actions" id="000">
      <div class="chat__message-body" contenteditable="true">Hello</div>
    </li>
  </ul>`;

  const expected = `
    <li class="chat__message actions edited" id="000">
      <div class="chat__message-body" contenteditable="true">Hello, Goodbye</div>
    </li>
  `;

  const chatView = document.getElementById('chatView');
  renderEditedMessage({ message: 'Hello, Goodbye', messageId: '000' });
  expect(chatView.innerHTML).toEqual(expected);
});

test('Renders same message if edited message is empty', () => {
  document.body.innerHTML = `
    <div id="007" class="actions">
      <div id="000" class="chat__message-body">message<div>
    </div>
  `;
  const messageContainer = document.getElementById('007');
  const messageBody = document.getElementById('000');
  const currentMessage = messageBody.innerHTML;
  messageBody.innerHTML = '';

  editMessage({
    currentMessage,
    messageContainer,
    messageId: '000',
    target: messageBody,
  });

  expect(messageBody.innerHTML).toEqual(currentMessage);
  expect(messageContainer.classList.contains('actions')).toEqual(false);
});

test('User is writting message', () => {
  document.body.innerHTML = '<div id="userActivity"></div>';

  const userActivity = document.getElementById('userActivity');
  const message = 'Someone is writting...';

  renderUserIsTypingMessage({ message });

  const expected = `<div id="userActivity">${message}</div>`;

  expect(userActivity.innerHTML).toEqual(message);
});

test('Render normal message of a user', () => {
  document.body.innerHTML = `<div id="chatView"></div>`;
  const client = 'Mock';
  const clientId = '007';
  const isBot = false;
  const message = 'Hello';
  const messageId = '001';
  const timestamp = '00:00';

  const expected = `<li class="chat__message" id="001" data-client="007"><div class="chat__message-header row"><div class="chat__message-info row"><span class="chat__message-info-user">Mock</span><span class="chat__message-info-timestamp">00:00</span></div><div class="chat__message-actions row"><span class="chat__message-actions-delete">🗑️</span></div></div><div class="chat__message-body">Hello</div></li>`;
  const chatView = document.getElementById('chatView');
  renderMessage({ client, clientId, isBot, message, messageId, timestamp });
  expect(chatView.innerHTML).toEqual(expected);
});

test('Render message from a bot', () => {
  document.body.innerHTML = `<div id="chatView"></div>`;
  const client = 'Mock';
  const clientId = '007';
  const isBot = true;
  const message = 'Hello';
  const messageId = '001';
  const timestamp = '00:00';

  const expected = `<li class="chat__message" id="001" data-client="007"><div class="chat__message-header row"><div class="chat__message-info row"><span class="chat__message-info-user">Mock</span><span class="chat__message-info-timestamp">00:00</span></div></div><div class="chat__message-body bot-message">Hello</div></li>`;
  const chatView = document.getElementById('chatView');
  renderMessage({ client, clientId, isBot, message, messageId, timestamp });
  expect(chatView.innerHTML).toEqual(expected);
});

test('Enable actions on user chat message', () => {
  sessionStorage.setItem(
    'user',
    JSON.stringify({ id: '000', name: 'Mock Mock' })
  );

  document.body.innerHTML = `<li class="chat__message" id="test" data-client="000">
  <div class="chat__message-header row">
    <div class="chat__message-info row">
      <span class="chat__message-info-user">Lucinda Thomas</span>
      <span class="chat__message-info-timestamp">20:50</span>
    </div>
    <div class="chat__message-actions row">
      <span class="chat__message-actions-delete">🗑️</span>
    </div>
  </div>
  <div class="chat__message-body">hola</div>
  </li>`;

  const target = document.getElementById('test');
  const messageBody = target.getElementsByClassName('chat__message-body')[0];

  enableActions.call(target, { target });

  const contentEditableAttribute = messageBody.getAttribute('contenteditable');

  expect(contentEditableAttribute).toEqual('true');
  expect(target.classList.contains('actions')).toBe(true);

  enableActions.call(target, { target });

  expect(target.classList.contains('actions')).toBe(false);
});

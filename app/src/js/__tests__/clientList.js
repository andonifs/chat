import {
  renderClientList,
  updateTotalOfParticipants,
} from '../components/ClientList';

test('Update number of participants in tab', () => {
  // Set up our document body
  document.body.innerHTML =
    '<div>' +
    'Participants' +
    "<span id='participantsCount'>(0)</span>" +
    '</div>';

  const count = 5;
  updateTotalOfParticipants(count);
  const participantsCount = document.getElementById('participantsCount');
  const expected = '(5)';

  expect(participantsCount.innerHTML).toEqual(expected);
});

test('Add updated list of clients to DOM, setting active-user class to the user stored in session storage', () => {
  sessionStorage.setItem(
    'user',
    JSON.stringify({ id: '007', name: 'Mock Mock' })
  );

  document.body.innerHTML = '<ul id="onlineUsers"></ul>';

  const clients = [
    { client: 'Mock Mock', clientId: '007' },
    { client: 'Other User', clientId: '008' },
  ];

  const userList = document.getElementById('onlineUsers');
  const expected =
    '<li class="user__item row active-user" id="007">' +
    'Mock Mock' +
    '</li>' +
    '<li class="user__item row" id="008">' +
    'Other User' +
    '</li>';

  renderClientList(clients);

  expect(userList.innerHTML).toEqual(expected);
});

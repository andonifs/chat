import { compose } from '../services/fn';
import { getUser, getUserId } from '../services/sessionStorage';
import { getHTMLCollection, htmlColletionToArray } from '../services/browser';
import { getRandomUser } from '../services/user';

test('Function composition', () => {
  const funcOne = (param) => param * 2;
  const funcTwo = (param) => param - 2;

  const result = compose(funcOne, funcTwo)(6);

  expect(result).toEqual(10);
});

test('Get values from sessionStorage', () => {
  sessionStorage.setItem(
    'user',
    JSON.stringify({ id: 'XYZ', name: 'Mock Mock' })
  );

  expect(getUser()).toEqual('Mock Mock');
  expect(getUserId()).toEqual('XYZ');
});

test('HTMLCollection to Array', () => {
  document.body.innerHTML = `<div class="inception">
    <div class="inception">
      <div class="inception"></div>
    </div>
  </div>`;

  const test = getHTMLCollection('inception');
  const array = htmlColletionToArray(test);

  expect(Array.isArray(array)).toBe(true);
  expect(array).toHaveLength(3);
});

test('Get random user', () => {
  const user = getRandomUser();
  const userTypeof = typeof user;
  expect(userTypeof).toEqual('string');
});

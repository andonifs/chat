//SESSION STORAGE
const USER_KEY = 'user';

// Socket connection
const WEB_SOCKET_ADDRESS = 'ws://192.168.1.123'; //For local testing change this value for your local IP
const WEB_SOCKET_PORT = 3001;
const WEB_SOCKET_URL = `${WEB_SOCKET_ADDRESS}:${WEB_SOCKET_PORT}`;

export { USER_KEY, WEB_SOCKET_URL };

import { connection } from './index';
import { getUser, getUserId } from '../../services/sessionStorage';
import {
  DELETE_ACTION_CONTENT,
  DELETE_MESSAGE,
  EDIT_MESSAGE,
  MESSAGE,
} from './constants';

const editMessage = ({
  client,
  currentMessage,
  messageContainer,
  messageId,
  target,
}) => {
  if (target.innerHTML !== currentMessage && target.innerHTML !== '') {
    messageContainer.classList.remove('actions');
    sendMessage({
      client,
      messageId,
      message: target.innerHTML,
      type: EDIT_MESSAGE,
    });
  }

  if (target.innerHTML === '') {
    messageContainer.classList.remove('actions');
    target.innerHTML = currentMessage;
  }
};

const sendMessage = (data) => {
  connection.send(JSON.stringify(data));
};

const renderDeletedMessage = ({ message, messageId }) => {
  const messageContainer = document.getElementById(messageId);
  const messageBody = messageContainer.getElementsByClassName(
    'chat__message-body'
  )[0];
  messageBody.classList.add('bot-message');
  messageBody.setAttribute('contenteditable', false);
  messageContainer.classList.remove('actions', 'edited');
  messageContainer.removeEventListener('click', enableActions);
  messageBody.innerHTML = message;
};

const renderEditedMessage = ({ message, messageId }) => {
  const messageContainer = document.getElementById(messageId);
  const messageBody = messageContainer.getElementsByClassName(
    'chat__message-body'
  )[0];
  messageContainer.classList.add('edited');
  messageBody.innerHTML = message;
};

const renderMessage = ({
  client,
  clientId,
  isBot,
  message,
  messageId,
  timestamp,
}) => {
  const chat = document.getElementById('chatView');

  // Create elements
  const messageContainer = document.createElement('li');
  const messageHeader = document.createElement('div');
  const messageInfo = document.createElement('div');
  const messageInfoClient = document.createElement('span');
  const messageTimeStamp = document.createElement('span');
  const messageBody = document.createElement('div');
  // Add classes to elements
  messageContainer.classList.add('chat__message');
  messageHeader.classList.add('chat__message-header', 'row');
  messageInfo.classList.add('chat__message-info', 'row');
  messageInfoClient.classList.add('chat__message-info-user');
  messageTimeStamp.classList.add('chat__message-info-timestamp');
  messageBody.classList.add('chat__message-body');
  // Add content to elements
  messageInfoClient.innerHTML = client;
  messageTimeStamp.innerHTML = timestamp;
  messageBody.innerHTML = message;
  // Add Id to elements
  messageContainer.id = messageId;
  messageContainer.dataset.client = clientId;
  // Append elements
  messageInfo.appendChild(messageInfoClient);
  messageInfo.appendChild(messageTimeStamp);
  messageHeader.appendChild(messageInfo);
  messageContainer.appendChild(messageHeader);
  messageContainer.appendChild(messageBody);

  // Create elements only for non bot messages
  if (!isBot) {
    // Create elements
    const messageActions = document.createElement('div');
    const messageDelete = document.createElement('span');
    // Add classes to elements
    messageActions.classList.add('chat__message-actions', 'row');
    messageDelete.classList.add('chat__message-actions-delete');
    // Add content to elements
    messageDelete.innerHTML = DELETE_ACTION_CONTENT;
    // Add actions
    messageContainer.addEventListener('click', enableActions, false);
    const currentMessage = messageBody.innerHTML;
    messageBody.addEventListener('blur', (e) => {
      editMessage({
        client,
        currentMessage,
        messageContainer,
        messageId,
        target: e.target,
      });
    });
    messageDelete.addEventListener('click', () => {
      sendMessage({ client, messageId, type: DELETE_MESSAGE });
    });
    // Append elements
    messageActions.appendChild(messageDelete);
    messageHeader.appendChild(messageActions);
  } else {
    messageBody.classList.add('bot-message');
  }

  // Append chat line
  chat.appendChild(messageContainer);

  //Keep scroll to bottom of chat view
  chat.scrollTop = chat.scrollHeight - chat.clientHeight;
};

// Mesage actions
const enableActions = function ({ target }) {
  const dataClient = target.dataset.client;
  const clientId = getUserId();
  const messageBody = this.getElementsByClassName('chat__message-body')[0];
  const parentClientId = messageBody.parentNode.dataset.client;

  if (parentClientId === clientId && !this.classList.contains('actions')) {
    this.classList.add('actions');
    messageBody.setAttribute('contenteditable', true);
  } else if (dataClient === clientId) {
    this.classList.toggle('actions');
  }
};

//User is typing action
const renderUserIsTypingMessage = ({ message }) => {
  const userActivity = document.getElementById('userActivity');
  userActivity.innerHTML = message;
};

export {
  editMessage,
  enableActions,
  renderDeletedMessage,
  renderEditedMessage,
  renderMessage,
  renderUserIsTypingMessage,
  sendMessage,
};

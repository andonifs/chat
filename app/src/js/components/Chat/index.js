import { getRandomUser } from '../../services/user';
import { getUser } from '../../services/sessionStorage';
import { CLIENT_LIST, SEND_MESSAGE, STOPPED_TYPING, TYPING } from './constants';
import { USER_KEY } from '../../app.config';
import { chatReducer } from './reducer';
import { v4 as uuidv4 } from 'uuid';
import { WEB_SOCKET_URL } from '../../app.config';
import { sendMessage } from './actions';

//Random client data
const client = getRandomUser();
const clientId = uuidv4();

// Store client id
sessionStorage.setItem(
  USER_KEY,
  JSON.stringify({ id: clientId, name: client })
);

// Create a socket instance
const connection = new WebSocket(WEB_SOCKET_URL);

connection.onopen = (event) => {
  const data = {
    client,
    clientId,
    type: CLIENT_LIST,
  };

  // Send user data to server
  connection.send(JSON.stringify(data));
};

// Handle messages from server
connection.onmessage = ({ timeStamp, data }) => {
  const message = JSON.parse(data);
  chatReducer(message);
};

// TODO Handle errors
connection.onerror = function (error) {
  console.log('WebSocket Error ' + error);
};

//Set listener to chat input
const setListenersToChatInput = () => {
  const input = document.getElementById('chatInput');

  if (input) {
    input.addEventListener('keydown', ({ target, keyCode }) => {
      if (keyCode === 13 && target.value !== '') {
        chatReducer({ type: SEND_MESSAGE, message: target.value, client });
        target.value = '';
      }
    });

    input.addEventListener('focus', () => {
      sendMessage({ client: getUser(), type: TYPING });
    });

    input.addEventListener('blur', () => {
      sendMessage({ client: getUser(), type: STOPPED_TYPING });
    });
  }
};

// Chat functions
setListenersToChatInput();

export { connection, setListenersToChatInput };

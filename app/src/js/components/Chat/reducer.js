import {
  CLIENT_LIST,
  DELETE_MESSAGE,
  MESAGE_EDITED,
  SEND_MESSAGE,
  TYPING,
} from './constants';
import { renderClientList, updateTotalOfParticipants } from '../ClientList';
import {
  renderDeletedMessage,
  renderEditedMessage,
  renderMessage,
  renderUserIsTypingMessage,
  sendMessage,
  storeUserId,
} from './actions';

const chatReducer = (action) => {
  switch (action.type) {
    case CLIENT_LIST:
      updateTotalOfParticipants(action.connectedClients.length);
      renderClientList(action.connectedClients);
      break;
    case SEND_MESSAGE:
      sendMessage({ message: action.message, client: action.client });
      break;
    case TYPING:
      renderUserIsTypingMessage(action);
      break;
    case DELETE_MESSAGE:
      renderDeletedMessage(action);
      break;
    case MESAGE_EDITED:
      renderEditedMessage(action);
      break;
    default:
      renderMessage(action);
  }
};

export { chatReducer };

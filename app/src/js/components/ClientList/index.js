import { getUserId } from '../../services/sessionStorage';

const updateTotalOfParticipants = (count) => {
  const participants = document.getElementById('participantsCount');
  participants.innerHTML = `(${count})`;
};

const renderClientList = (items) => {
  const userList = document.getElementById('onlineUsers');
  userList.innerHTML = ''; // Remove all participants

  // Append participants
  items.forEach(({ client, clientId }) => {
    const itemElement = document.createElement('li');
    itemElement.classList.add('user__item', 'row');

    if (clientId === getUserId()) {
      itemElement.classList.add('active-user'); // Add class to current user
    }

    itemElement.id = clientId;
    itemElement.innerHTML = client;
    userList.appendChild(itemElement);
  });
};

export { renderClientList, updateTotalOfParticipants };

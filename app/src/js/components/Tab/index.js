import {
  getHTMLCollection,
  htmlColletionToArray,
} from '../../services/browser';
import { compose } from '../../services/fn';
import { INITIAL_TAB } from './config';

const setActiveTab = (selectedTab) => {
  selectedTab.classList.add('active');
  return selectedTab;
};

const renderActiveTab = ({ dataset: { tab } }) => {
  const content = document.getElementById(tab);
  content.classList.add('visible');
};

const createListener = (fn) => {
  let selectedTab = INITIAL_TAB;
  return (tabs) => {
    tabs.forEach((tab) => {
      tab.addEventListener('click', (e) => {
        // Only run function if tab hasn't been clicked
        if (tab.dataset.tab === selectedTab) {
          return;
        }
        selectedTab = tab.dataset.tab;
        return fn(e, tabs);
      });
    });
  };
};

const onTabChange = ({ target: selectedTab }, tabs) => {
  const tabsContent = compose(
    getHTMLCollection,
    htmlColletionToArray
  )('tab__content');

  tabs.forEach((tab) => tab.classList.remove('active'));
  tabsContent.forEach((tab) => tab.classList.remove('visible'));

  compose(setActiveTab, renderActiveTab)(selectedTab);
};

const addClickListenerToTabs = createListener(onTabChange);

//Compose event listener on tabs
const tabs = compose(
  getHTMLCollection,
  htmlColletionToArray,
  addClickListenerToTabs
)('tab__menu-item');

const getHTMLCollection = (className) => {
  return document.getElementsByClassName(className);
};

const htmlColletionToArray = (htmlCollection) => Array.from(htmlCollection);

export { getHTMLCollection, htmlColletionToArray };

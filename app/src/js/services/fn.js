const compose = (...args) => (input) => {
  const reducer = (input, fn) => fn(input);
  return args.reduce(reducer, input);
};

export { compose };

import { USER_KEY } from '../app.config';

const getItem = (item) => {
  return JSON.parse(sessionStorage.getItem(item));
};

const getUser = () => getItem(USER_KEY).name;
const getUserId = () => getItem(USER_KEY).id;

export { getUser, getUserId };

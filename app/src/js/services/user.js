const getRandomUser = () => {
  const names = [
    'Lucinda',
    'Fito',
    'Don',
    'Vic',
    'Laura',
    'Chadwick',
    'Mark',
    'Anjani',
    'Leonard',
    'Bill',
    'Ty',
    'Gillian',
    'Jeff',
    'Andrew',
    'Lou',
  ];
  const surnames = [
    'Stokes',
    'Buckley',
    'Withers',
    'Sandman',
    'Reed',
    'Thomas',
    'Knopfler',
    'Williams',
    'Wood',
    'Marling',
    'Chesnutt',
    'Welch',
    'Segall',
    'Paez',
    'Cohen',
  ];

  const randomNameNumber = Math.round(Math.random() * (names.length - 1));
  const randomSurnameNumber = Math.round(Math.random() * (surnames.length - 1));

  const randomName = names[randomNameNumber];
  const randomSurname = surnames[randomSurnameNumber];

  return `${randomName} ${randomSurname}`;
};

export { getRandomUser };

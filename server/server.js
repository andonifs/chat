'use strict';

const { v4: uuidv4 } = require('uuid');
const WebSocket = require('ws');
const {
  BOT_NAME,
  CHAT_BOT_MESSAGE,
  CLIENT_LIST_MESSAGE,
  DELETE_MESSAGE,
  DELETE_MESSAGE_RESPONSE,
  EDIT_MESSAGE,
  MESAGE_EDITED,
  STOPPED_TYPING,
  TYPING,
  WEB_SOCKET_URL,
  WEB_SOCKET_PORT,
} = require('./server.config.js');

let connectedClients = [];

// Websocket actions
const wss = new WebSocket.Server({
  localAddress: WEB_SOCKET_URL,
  port: WEB_SOCKET_PORT,
});

wss.broadcast = (data, sender) => {
  wss.clients.forEach(function each(client) {
    client.send(data);
  });
};

wss.broadcastToClients = (data, sender) => {
  wss.clients.forEach(function each(client) {
    if (sender && client !== sender) {
      client.send(data);
    }
  });
};

wss.on('connection', (ws, a) => {
  ws.on('message', (obj) => {
    const data = JSON.parse(obj);
    switch (data.type) {
      case CLIENT_LIST_MESSAGE:
        ws.id = data.clientId;
        addOnlineClient(data, ws.id);
        sendClientListUpdated(ws.id);
        sendBotMessage(`${data.client} joined`);
        break;
      case DELETE_MESSAGE:
        sendMessage({
          ...data,
          message: DELETE_MESSAGE_RESPONSE,
          messageId: data.messageId,
          isDeleted: true,
          clientId: ws.id,
        });
        break;
      case EDIT_MESSAGE:
        sendMessage({
          ...data,
          messageId: data.messageId,
          isEdited: true,
          clientId: ws.id,
          type: MESAGE_EDITED,
        });
        break;
      case TYPING:
        sendMessage(
          {
            ...data,
            message: `${data.client} is writting...`,
            clientId: ws.id,
            type: TYPING,
          },
          ws
        );
        break;
      case STOPPED_TYPING:
        sendMessage(
          {
            ...data,
            message: '',
            clientId: ws.id,
            type: TYPING,
          },
          ws
        );
        break;
      default:
        sendMessage({ ...data, clientId: ws.id });
    }
  });

  ws.on('close', () => {
    const disconnectedClient = removeOnlineClient(ws.id);
    sendClientListUpdated();
    sendBotMessage(`${disconnectedClient} left`);
  });
});

const addOnlineClient = ({ client }, clientId) => {
  connectedClients = [...connectedClients, { client, clientId }];
  return connectedClients;
};

const removeOnlineClient = (clientId) => {
  let disconnectedClient;
  connectedClients = connectedClients.filter((item) => {
    if (item.clientId !== clientId) {
      return true;
    }
    disconnectedClient = item.client;
  });
  return disconnectedClient;
};

const sendClientListUpdated = (clientId) => {
  wss.broadcast(
    JSON.stringify({
      clientId,
      connectedClients,
      type: CLIENT_LIST_MESSAGE,
    })
  );
};

const sendMessage = (
  {
    client,
    clientId,
    message,
    messageId,
    isEdited = false,
    isDeleted = false,
    type,
  },
  sender
) => {
  const data = JSON.stringify({
    client,
    clientId,
    isDeleted,
    isEdited,
    message,
    messageId: messageId || uuidv4(),
    timestamp: new Date().toLocaleTimeString([], {
      hour12: false,
      hour: '2-digit',
      minute: '2-digit',
    }),
    type,
  });
  if (sender) {
    wss.broadcastToClients(data, sender);
  } else {
    wss.broadcast(data);
  }
};

const sendBotMessage = (message) => {
  wss.broadcast(
    JSON.stringify({
      type: CHAT_BOT_MESSAGE,
      client: BOT_NAME,
      isBot: true,
      message,
      messageId: uuidv4(),
      timestamp: new Date().toLocaleTimeString([], {
        hour12: false,
        hour: '2-digit',
        minute: '2-digit',
      }),
    })
  );
};
